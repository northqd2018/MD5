# MD5

#### 介绍
从OpenSSL-1.0.2r源码中移植的C语言MD5算法

公司项目要用到MD5。看到OpenSSL源码的Crypto/Md5目录下有md5.c、md5.h文件，
以为作者们把MD5封装到了这两个文件中，可直接拷贝使用，其实不然，
于是从各个文件中剥离出了相关内容，封装到自己的md5.c、md5.h中。

目前在32位环境中正常运行。

#### 使用说明

md5.h第70行，有一个确定大小端模式的宏，根据实际情况定义，
`DATA_ORDER_IS_BIG_ENDIAN`或`DATA_ORDER_IS_LITTLE_ENDIAN`，
如果MD5计算结果不对，可能是这个宏和实际情况不符。

```C
/*
 * DATA_ORDER_IS_BIG_ENDIAN or DATA_ORDER_IS_LITTLE_ENDIAN
 * It must be determined according to the actual situation.
 */
#define DATA_ORDER_IS_LITTLE_ENDIAN
```

示例代码如下：

```C
#include "md5.h"

int main(void)
{
    MD5_CTX ctx;
    unsigned char test_data[] ="test123";
    unsigned char res[16];
    
    MD5_Init(&ctx);
    MD5_Update(&ctx, test_data, 7);
    MD5_Final(res, &ctx);  /* MD5结果保存在res中 */
    
    return 0;
}
```

如果是8位机，md5.h中`MD5_LONG`和`MD32_REG_T`要改成long型，暂未实际测试。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
